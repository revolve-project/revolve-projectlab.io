+++
title = "Channels"
template = "page.html"
sort_by = "weight"
weight = 2
+++

# Channels
Revolve channels transport items between `Components`. Items usually are
paths to files, but can also be abstract values encoded as strings. The config
channel has predefined files or values and can be seen as the root channel.

Using an output channel as input does not consume the items. Multiple
components can depend on the same channel and will still receive all the
items. Additionally channel items will be recycled when necessary if multiple
in-channels are used that are of different length.

## In
In-channels take data from either out-channels of other components, or from
the config-channel, forcing a hierarchical relation. To get the stream of items
into a form that can be used in the command template, several operations are
available.

### Each
`each(<out-channel>)`

Use `each(<channel>)` to reference one item per iteration from the out channel.
Command template will be run exactly the length of the `<channel>` times.

### All
`all(<out-channel>)`

Use all items from the out channel at once. The output is independent of the
number of `<in-channel>` items, and relate to all in channel items.

### Pairs
`pairs(/<regex>/, <out-channel>)`

Use the regular expression to group items in the channel into pairs. The regular
expression should contain a single capture group (use parentheses). Grouping
using the captured text must result in pairs.

### Tuples and vartuples
`tuples(/<regex>/, <out-channel>, size)`, `vartuples(/<regex>/, <out-channel>)`

Like pairs, but tuples specify the number of results that should be grouped
using the capture results. `vartuples` do the same, but do not assume an evenly
sized output, but provide the channel items the regex captures,

### Combinations
`combinations(<out-channel1>, <out-channel2>)`

Create the Cartesian product of two channels. A tupleindex of .0 references the
first channel, .1 the second. The number of tasks run is the product of the
lengths of the two channels.


## Out
Out channels are used to specify the items produced by the component, usually
a list of files. It can be a single entry, but it is more common to apply a
transformation to an in-channel and get a dynamic output name for the
component.

The commands that can be applied to items in the components' in-channels are:

### Sub
`sub(/<regex>/, '<replacement>', <in-channel>[.tuple_index])`

Replacing a part of the in file to generate outfiles is a very useful method to
generate new items. If the in-channel uses pairs/tuples/vartuples/combinations
a tuple index (.0, .1, ..) allows to select which entry to use.

### Basename
`basename(<in-channel>)`

This command drops the leading path and yields only the file name part. The
extension is kept.

### Dirname
`dirname(<in-channel>)`

This command drops the file name and yields the path of the item. Modification
time of directories differ from files, therefore revolve cannot evaluate a
directories up-to-date status. Directory names function as a value type.

### Append
`append(<in-channel>, '<value>')`

Use this command to append a string to every item in the in-channel

## Ref
A ref represents a value type that only exists within a component and can be
referenced by out channels or other refs. It is useful to keep string
concatenations readable, otherwise follows the same syntax of out channels.
A ref is not truly a channel and cannot be used as a placeholder in the command
section.

```
[comp.aln]
in.fa_fq = "combinations(config.fasta, config.fastq)"
ref.fa = "sub(/(.*).fasta$/, '$1', in.fa_fq.0)"
out.bam = "sub(/.fastq.gz$/, '_#{ref.fa}.bam', in.fa_fq.1)"
# ...
```
