+++
title = "Pipeline documents"
template = "page.html"
sort_by = "weight"
weight = 1
+++
# Pipeline
A Revolve pipeline is written as a `TOML` document. The processing of the
pipeline definition follows the following steps:
 * Reading the TOML document
 * Interpreting the document contents and building the `Components`
 * Order the components to satisfy the in- and out requirements
 * Applying the commands provided in the `Config` section which should provide
   the `Items` the pipeline will operate on.
 * Apply the in and out `Channel`s of the ordered components to generate a list
   of items the pipeline will produce
 * Insert these items in the template provided in the `command` section
   resulting in a `Task`
 * Create an internal representation linking all tasks and all items in graphs.

 After validation and processing the pipeline can be reviewed, after dry-running
 or running.

# Config
Every item specified in the `Config` is interpreted as an out-channel. The
values or files referenced there can be used in the pipeline. It usually
contains the input data, but can also contain specific versions of files that
change relatively often, or even command line switches that are applied in
specific component's command.

Config variables are one of three types
 * A path to a file or a directory
 * A text variable
 * A command

## Config commands
In the configuration section the following commands are available:
 * `file('<path>')`
 * `glob('<pattern>'[, '<pattern>'..])`
 * `filetable('<path>')`
 * `<value>`
 * `table('<path>')`
 * `append('value', <prior_config>)`
 * `sub(/<regex>/, 'replacement', <prior_config>)`

Use the file() command to select a single pre-existing file. The pipeline will
not run unless the file is present.

Use the glob() command to select a set of files that this channel will emit.
`glob` supports the Unix file globbing syntax, e.g. `/path/**/*.txt` to
recursively search for `.txt` files.

A filetable() is a list of pre-existing files, all of which must exists.

If a string does not match any command, it is interpreted as a value type. The
value type is no dependency itself, but can be referenced as placeholders in
components or other config strings.

Use table() to define a list of value types. A tupleindex, at the end of a
placeholder - .0, .1, etc. - can be used to indicate which respective element
of the list the placeholder does reference.

Append() appends the given string value to prior config value. If the result
hereof references a pre-existing file, the result becomes a file dependency.

A sub() command can be used to substitute-replace one value or file type into
another, which similarly may become a file dependency. Regular expression rules
as specified in https://docs.rs/regex/latest/regex/, a double backslash is
required for escape sequences.


## Example
```TOML
[config]
fastqfiles = "glob('/my-data-repository/my-sequencing-results/**/*.fastq.gz')"

build = "GRCh38"
ref = "file('/home/sulston/Homo_sapiens.#{build}.dna.primary_assembly.fa')"
adapters = "table('forward_and_reverse_adapters.txt')"
fai = "append('.fai', config.ref)"
dict = "append(/\\.fai$/, '.dict', config.fai)"
```

# Components
A component represents a functional step in the pipeline. To Act on specific
files it has in- and out-channels and a command, a shell template referencing
these files via their channels using placeholders for the commands it executes.
There are also parameters that define how the component is run.

## Channels
Documented in detail in [Channels](@content/docs/channels.md#Channels) the
in- and out channels define what files the component will read from or write to.

### In channels
Organize how file tasks are grouped and organized between components, what
files will be read. Writing to an in channel is incorrect. See [In](@content/docs/channels.md#In).

### Out channels
Handle the naming and renaming of files, and define what files the component
will write to. See [Out](@content/docs/channels.md#Out).

### Command
The shell script, run to create the files specified as out-channels. Not
updating those files results in a components task error, which will prevent all
downstream components that depend on this component from running. Determined by
how in-channels are grouped, the command template is spawned as one or more
tasks. In a command, Channels are specified via placeholders, in the format
`#{channel.name}`, where channel may be one of in-, out- or config.

## Parameters
Component parameters define how, under what conditions the commands are run.
Whether its output files are intermediate, soft requirements, or final results.
In what shell environment commands run, and with how many threads are defined
by parameters as well.

### Example
```TOML
# by dafault 1: no concurency, this uses two threads.
parallel = 2

# By default false. If all output is an intermediate step, only, set:
intermediate = true

# create and activate a conda environment with this software, to run the commands:
conda.requirements = ["samtools", "bwa"]

# alternatively, use an already pre-created conda environment:
conda.use_env = "/home/sulston/conda/bwa_aln"
```

### Parallel
Allow file tasks for this component's command to run concurrently as specified.
Assigning more threads than processors are available, is ineffective. Also a
task that reads all in channels of another component only, can never run on
more threads. Assigning multiple processes that may consume more memory than
there is available can be dangerous, so use parallel conservatively. See [OoM](@content/docs/troubleshooting.md#Out-of-memory).

### Intermediate
Set to true, if the output of this component is obsolete once downstream files
have been created. This may allow removing these intermediate files, preventing
unnecessary recreation upon a rerun of the pipeline. HOWEVER, always test with
`--dry-run` that indeed the recreation step is omitted, or downstream files
will be recreated as well. Intermediate only applies to a file output from file
source, and not for value types, see [troubleshooting intermediate](@content/docs/troubleshooting.md#Intermediate).

### Conda
If the command requires specific software, a Conda environment can be set up
with the software required to run the command. The software required is
defined using `conda.requirements = ["package1", "package2", ..]`. If there
already is an environment, use instead `conda.use_env = "/path/to/env"`.
Conda environments can be difficult to maintain with many packages. If
possible use many small dedicated environments.
