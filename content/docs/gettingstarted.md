+++
title = "Getting started"
template = "page.html"
sort_by = "weight"
weight = 0
+++
# What is Revolve
Revolve is a (bioinformatics) pipeline runner. Using a small amount of
configuration input and set of linked components Revolve will generate the
dependency graph, check for outdated files and (re-)run script snippets if
required. Revolve does parallel execution, dry-running, visualization and
reporting.

# Installation
There are three ways to install revolve.
 - Build from source
 	- install Rust: https://rustup.rs
	```bash
	$ git clone https://
	$ cargo build --release`
	$ ./target/release/revolve
	```
 - Given a working Rust programming environment, use `cargo`:
 	WIP: not yet submitted
	```bash
	$ cargo install revolve
	```
 - Revolve is also (WIP: not yet submitted) available on bioconda:
	```bash
	$ conda install revolve
	```

# Example pipeline
A minimal `Pipeline` is a `TOML` document that contains an id field and at
least two tables, `config` and `comp`. The `comp` table contains only nested
tables specifying the pipeline's `Components`.
```TOML
id = "my-pipeline"

[config]
textfiles = "glob('/tmp/pathwithdata/*.txt')"
re = "^chrX"

[comp.first]
in.txt = "each(config.textfiles)"
out.selection = "sub(/.txt$/, '.out', in.txt)"
command = '''
grep #{config.re} #{in.txt} > #{out.selection}
'''
```

The configuration section provides a way to select files for processing. The
`glob` function selects files matching the pattern and puts them in a `Channel`.
See [Configuration](@content/docs/configuration.md#Configuration) for a more
detailed description about the `config` section and [Channels](@content/docs/channels.md)
for more information about channels and their available commands.

The table `first` specifies our pipeline's action. A `Component` has 3
mandatory fields. One or more `in` channel(s) referencing items from either the
config section _or_ previous component's out channels. One or more `out`
channel(s) specifying the items produced by this component.

In this example the in-channel named `txt` references the files from the config
out-channel `textfiles` in a one-by-one fashion (due to the `each` function).
The out-channel named `selection` creates new items by applying a
substitution on the items available in `in.txt`. The rules of the rust [regex](https://docs.rs/regex/%2A/regex/#syntax)
crate apply. Beside sub, the channel operations append, dirname and basename are
available.

The `command` is a single or multi-line template string. It should contain
placeholders that will be substituted by the elements available in the component
channels. When everything checks out the commands will be executed using
`Bash`.

If the command requires specific software, a conda environment can be set up
with the software required to run the command. The software required is
defined using `conda.requirements = ["package1", "package2", ..]`. If There
already is an environment, use instead `conda.use_env = "/path/to/env"`.


In practice a pipeline will consist of many components that can have complex
interactions.

# Running
Test a configuration for mistakes and get a list of the components that will be
executed by running:
```bash
$ revolve --dry-run pipeline.toml
```

It is good practice to always dry-run first. If a dry run reports errors, see
the [syntax errors](@content/docs/troubleshooting.md#Syntax). If all seems well,
run without `--dry-run`. An incorrectly configured pipeline can also have
runtime errors, if commands failed or files were not created as configured.
Then see [runtime errors](@content/docs/troubleshooting.md#Runtime).
