+++
title = "Troubleshooting"
template = "page.html"
sort_by = "weight"
weight = 1
+++
# General questions:

## output

### What will my pipeline do exactly?
Use `--dry-run`. For more details, to understand the interdependencies, there
is `--dry-run --log-level=debug`.

### Can I make a graph of the pipeline?
Yes! To make a graphical representation use `--pipeline-dot=<pipe.dot>` or
`--item-dot=<item.dot>`, and then e.g. `dot -Tpng pipe.dot > pipe.png`.

### Where is the standard/error output of a task? How can I review what tasks were run?
Revolve writes a directory `.revolveout/` wherein `<component>-<nr>.stdout` and
`<component>-<nr>.stderr` are written for each task. The first tasks are
executed in the order specified, glob or filetable order. See prior logging to
see what task corresponded to what stderr file, to review the error messages.
Component commands are run with `set -x`, which causes commands that are
executed to be logged to stderr.

## Intermdiate
### A component will not run
Was the component marked as intermediate? Those will only run if downstream
components depend on it, are not intermediate nor already up-to-date (have all
files already accounted for, and 'newer').

### Intermediate component runs although it should not
Check that downstream non-intermediate files are all present and up-to-date,
if this seems the case, the up-to-date state might not be evaluable, because
the file produced is not a file type, but a value type. Where appropriate, make
sure to use  `file('..')` or `filetable('..')` instead of `table('..')` in the
config section. Even after passing channels a value type stays a value type,
which renders the result of components not suitable to test up-to-date status.

If not helping either, just comment out the section in the toml.

### Out of memory
It is important to realize that commands that do not share memory, will also
multiply memory consumption. If consumption exceeds available RAM,
a computer may run [Out of memory](https://en.wikipedia.org/wiki/Out_of_memory),
which may have undesirable effects, like crashing computer processes, not
neccesarily revolve. Be careful and conservative with computer memory.

# Syntax

## Generic TOML Syntax errors
TOML syntax errors are reported in the format:
```
Parsing pipeline file x.toml
Pipeline error
Caused by:
    0: Error parsing pipeline TOML
    1: <reason> at line x column y
```

Some of the reasons reported can be:

### Invalid escape character in string `z`
A single backslash was used, which is not allowed in double quoted strings. Use
two backslashes or single quotes, or triple single quotes for commands.

### Invalid number
A number was written where a string was expected. Maybe double quote it?

### Other reasons
For more syntax errors review the [TOML specification](https://toml.io/en/).

## Revolve specific syntax errors
### Glob pattern `glob("<path>")` matched no files
Is the path correct, are they files, right permissions? Think!

### Dependency errors: X requires configuration value: config.Y
### Component 'X': Command template references non-existing config item: Y
In the command of component `[comp.X]` a placeholder `#{config.Y}` is used, but
in the `[config]` section, `Y = ..` is missing.

### Component 'X': Command template references non-existing in-channel: Y
### [comp.X]: Command template references non-existing in-channel item: Y
In the command of component `[comp.X]` a placeholder `#{in.Y}` is used, but
there was no `in.Y = ..`.

### Component 'X': Command template references non-existing out-channel: Y
In the command of component `[comp.X]` a placeholder `#{out.Y}` was used that
was not assigned with `out.Y = ..`.

### Error in channel, duplicated output: Output = input: `<file>`
A sub() command's regular expression did not match for an input filename, or an
empty string was appended to an input filename. As a security precaution,
      revolve does not allow reading from a file and writing to that same file.

### [comp.X]: channel error: Regex mismatch: Tuples of 2 or pairs expected.
In component X you have an `in.<name> = "pairs(/^<regex>/, [out.<comp>. or config.<name>])"`
but the regexp does not capture 2 files for all, verify the regex matches.
If there are only pairs of some, use vartuples(). If there are more than 3 of
all, use tuples().

### [comp.X]: channel error: Regex mismatch: Tuples of `<nr>` expected.
Likewise, you have an `in.<name> = "tuples(/^<regex>/, [out.<comp>. or config.<name>], <nr>)"`
where the number of files matched does not for all amount the the given number.

## Runtime
If you are already in a conda environment, first `conda deactivate` before
running revolve, to allow components to activate their environments.

### Cannot acquire lock on file '.revolve.lock'..
While the pipeline is running, a lock file is written, `.revolve.lock`, by
default. After completion the file is removed, unless interrupted; then remove
it manually to run again. The lock file prevents running a pipeline twice in the
same directory by accident. If a second pipelines will not interfere, override
with `--lock-file=<alt_lock>`.

### Task `X.<nr>` not successful..
An executed command failed or an output file was not written. There should be a
more specific notice of the error, see the more specific error messages below.

A pipeline may keep running, even after a failed task, for parallel tasks up to
that point. Sometimes it is fine to let the pipeline run, and continue later.
Sometimes it is better to Ctrl+C, clean up, including .revolve.lock; In most
cases the pipeline will need some modifications before it can be rerun.

Also be sure to first `--dry-run`, to ensure unnecessary processes are not
rerun. Adapt components with `intermediate = true` where necessary, to prevent
those from rerunning.

### Pipeline execution not successful. The following failures were reported: ..
### X:Shell task failed, received non-zero exit code
If unclear, for the first task `X.<nr>` that failed, look up the `X-<nr>.stderr`
reported by revolve on the command line. In the .revolveout directory, review
the reported `X-<nr>.stderr`. At the end is listed what command was run and any
error it reported before the execution halted.


### ExistingFile("X") -> ResultFile("Y") is not up to date!
The input file was modified later than the output file, in which case revolve
considers the task not to have run properly. There are several possible causes:
 * The output file, defined by the out-channel was already there, and indeed,
   it was not written to. Check that you named your out file correctly.
 * You are writing or modifying an in-channel file. This is something a command
   template should not do. An incorrect component command.
 * In-channel files were removed in the command. If the files are intermediate,
   a cleanup could make sense, but reading from and then deleting an inchannel
   disables revolve to test whether its output files were up-to-date, created
   after the source, and will cause task to fail, currently. Their removal
   could be postponed to a downstream task.
 * Multiple processes write to the same file, either overwriting or updating the
   file. To collect output, instead create a downstream component that gathers
   output, with an in-channel: `in.<name> = "all(<component>.<name>)"`.

### Shell task failed. Expected output not found
Not all of the expected output was generated, maybe the file name differed from
the one specified.
